package harjoitustyo.dokumentit;

/**
 * Vitsi-luokka, perii Dokumentti-luokan.
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
public class Vitsi extends Dokumentti {
   /** Laji-attribuutti. */
   private String laji;
   
   /*
    * Kolmiparametrinen rakentaja
    */
   public Vitsi(int uusiTunniste, String uusiLaji, String uusiTeksti) throws IllegalArgumentException{
       super(uusiTunniste, uusiTeksti);
       laji(uusiLaji);
   }


   /* 
    * Aksessorit.
    *
    */
   public String laji() {
      return laji;
   }

   public void laji(String uusiLaji) throws IllegalArgumentException {
       if (uusiLaji == null || uusiLaji.length() < 1) {
          throw new IllegalArgumentException();
       }
       laji = uusiLaji;
   }
   /**
    * Korvattu toString-metodi
    * 
    * @return Vitsi-luokan merkkijonoesitys.
    */ 
   @Override
   public String toString() {

      // Muodostetaan merkkijonoesitys. Object-luokan metodia ei kutsuta,
      // jotta esitys pysyisi selkeämpänä.
      return tunniste() + "///" + laji + "///" + teksti();
   }    
}

