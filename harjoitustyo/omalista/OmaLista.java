package harjoitustyo.omalista;

import harjoitustyo.apulaiset.Ooperoiva;
import harjoitustyo.dokumentit.Dokumentti;
import harjoitustyo.dokumentit.Uutinen;
import harjoitustyo.dokumentit.Vitsi;
import java.util.LinkedList;
/**
 * OmaLista-luokka, perii LinkedList-luokan. Toteuttaa Ooperoiva-rajapinnan.
 * 
 * OmaLista-tyyppiselle oliolle voidaan lisätä dokumentteja. 
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
public class OmaLista<E> extends LinkedList<E> implements Ooperoiva<E> {

   /**
    * Lisää listalle uuden alkion sen suuruusjärjestyksen mukaiseen paikkaan.
    * 
    * @param uusi viite olioon, jonka luokan tai luokan esivanhemman oletetaan
    * toteuttaneen Comparable-rajapinnan.
    * @throws IllegalArgumentException jos lisäys epäonnistui, koska uutta alkiota
    * ei voitu vertailla.
    */
   @SuppressWarnings({"unchecked"})
   @Override
   public void lisää(E uusi) throws IllegalArgumentException {
      
      if (uusi != null && (uusi instanceof String || uusi instanceof Integer || 
          uusi instanceof Vitsi || uusi instanceof Uutinen)) {
      
      // Lippumuuttuja sijoittamista varten
      boolean sijoitettu = false;
      // Tarkastetaan, onko lista tyhjä, jos on lisätään suoraan listalle.
      if (isEmpty()) {
         add(0, uusi);
         // Käännetään lippumuuttuja jotta samaa alkiota ei lisättäisi uudelleen.
         sijoitettu = true;
      }
      else if (size() > 0) {
         // Alustetaan silmukkalaskuri.
         int ind = 0;
         // Silmukan sisällä vertaillaan ja lisätään alkiot listään.
         while (!sijoitettu && ind < size()) {
            // Haetaan vertailtavat alkio, sijoitetaan muuttujiin.
            Comparable edellinen = (Comparable)get(ind);
            Comparable uusiVrt = (Comparable)uusi;
            int vrt = uusiVrt.compareTo(edellinen);

  
            // Jos vertailun tulos on 0 tai pienempi, sijoitetaan alkio
            // silmukkalaskurin määräämään paikkaan.
            if (ind < (size()) && vrt == 0) {
               int ind2 = ind;
               // Jos löytyy vastaavuus, katsotaan ovatko seuraavat alkiot 
               // myös vastaavia ja lisätään vasta niiden jälkeen
               while (!sijoitettu && ind2 < size()) {
                  Comparable sama = (Comparable)get(ind);
                  Comparable uudempi = (Comparable)uusi;
                  int vrt2 = uudempi.compareTo(sama);
                  if (vrt2 != 0) {
                     add(ind + 1, uusi);
                     sijoitettu = true;
                  }
                  ind2++;
               }
            }
            // Jos vertailu -1, lisätään laskurin määrittämään paikkaan.
            if (ind < (size()) && vrt < 0) {
               add(ind, uusi);
               // Käännetään lippumuuttuja jotta ajoa ei jatkettaisi.
               sijoitettu = true;
            }
            // Jos silmukka ajaa viimeiseen alkioon asti,
            // lisätään alkio viimeiseksi.
            if (ind == (size() - 1)) {
               addLast(uusi);
               // Käännetään lippumuuttuja jotta ajoa ei jatkettaisi.
               sijoitettu = true;
            }
            // Kasvatetaan laskuria
            ind++;       
         }
      }
   }
      else {
         throw new IllegalArgumentException("Virheellinen parametri!");
      }
   }
}
