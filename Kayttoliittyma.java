
import harjoitustyo.dokumentit.Dokumentti;
import harjoitustyo.kokoelma.Kokoelma;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Käyttöliittymä-luokka jonka sisällä tapahtuu käyttäjän ja ohjelman 
 * välinen vuorovaikutus. Kutsuu Kokoelma-luokan metodeja.
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
public class Kayttoliittyma { 
   /** Viite lukijaan. */
   public static final Scanner lukija = new Scanner(System.in);
   
   /** Mahdolliset komennot ja erottimet vakiona. */
   public static final String TULOSTA = "print";
   public static final String LISAA = "add";   
   public static final String ETSI = "find";  
   public static final String POISTA = "remove"; 
   public static final String ESIKASITTELE = "polish";   
   public static final String RESETOI = "reset"; 
   public static final String LOPETUS = "quit";
   public static final String EROTIN = " ";
   public static final String KAIKU = "echo";
   public static final String DOKUEROTIN = "///";
   /** Viite kokoelmaan. */
   public static Kokoelma korpus;
   /** Viite sulkusanoille. */
   public static LinkedList<String> sulkusanat;
   /** Viite hakusanoille */
   public static LinkedList<String> hakusanat;
   /** 
    * Metodi käyttäjän antaman syötteen rakenteelliseen tarkastukseen
    *
    * @param syote käyttäjän antama syöte merkkijonona.
    * @return null, jos syöte on virheellinen, komento, jos syöte on kunnossa.
    */  
   public static String[] tarkistaSyote(String syote) {
  
      // Palautetaan heti null jos syote on null tai sen pituus on 0
      if (syote == null || syote.length() == 0) {
         return null;
      }
      
      // Jaetaan syöte osiin ja sijoitetaan yksiulotteiseen taulukkoon
      String[] komento = syote.split(EROTIN, 2);
      
      
      // Jos komento on joku muu kuin dilaatio tai eroosio ja
      // sillä ei ole parametrejä, palautetaan tarkistettu komento
      if (komento[0].equals(TULOSTA)){
         return komento;
      }
      
      if (((komento[0].equals(RESETOI)) || (komento[0].equals(LOPETUS)) 
      || (komento[0].equals(KAIKU))) && komento.length == 1){
         return komento;
      }
      
      else if (((komento[0].equals(LISAA)) || (komento[0].equals(ETSI)) 
      || (komento[0].equals(POISTA)) || (komento[0].equals(ESIKASITTELE))
      || (komento[0].equals(TULOSTA))) && (komento.length > 1 && komento.length < 3)) {
         return komento;
      }
      // Syötteen rakenne ei ole kunnossa, palautetaan null
      return null;
   }
   
   /** 
    * Oope2HT-ajoluokan päämetodi. 
    * @param nimi, tekstitiedoston nimi, sulkusanaNimi, sulkusanatiedoston nimi
    * Käyttäjän ja ohjelman välinen vuorovaikutus pääasiallisesti tämän sisällä.
    */     
   public static void liittyma (String nimi, String sulkusanaNimi) {
      
      korpus = new Kokoelma();
      korpus.lataaKokoelma(nimi);
      
      sulkusanat = new LinkedList<String>();
      sulkusanat = korpus.lataaSulkusanat(sulkusanaNimi);
      // Jos metodi palauttaa tyhjäarvon, viestitään virheellisestä tiedostosta
      if (korpus.dokumentit() == null || sulkusanat == null) {
         System.out.println("Missing file!");
      }
         
      // Kuva on ladattu taulukkoon onnistuneesti, jatketaan ajoa
      else { 
         // Alustetaan epätosi lippumuuttuja echo-komennon käsittelyä varten
         boolean onkoKaiku = false;
         int echoLaskuri = 0;
         // Lippumuuttuja joka kontrolloi pääsilmukan ajoa, epätosi vain komennolla 'quit'
         boolean jatkaAjoa = true;
         int tunniste = 0;

         // Pääsilmukka, käyttäjän ja ohjelman välinen vuorovaikutus tapahtuu tämän sisällä
         do {
            try {
               // Tiedustellaan käyttäjältä komento
               System.out.println("Please, enter a command:");
               String syote = lukija.nextLine();
               // Jos echo havaittu, tulostetaan käyttäjän syöte välittömästi näytölle
               if (onkoKaiku) {
               System.out.println(syote);
               }
               
               // Tarkistetaan syötteen rakenne, jaetaan osiin.
               String[] komento = tarkistaSyote(syote);
                  
               // Muiden kuin add ja polish toimintojen kohdalla sijoitetaan 
               // komennon toinen parametri kokonaisluvuksi.
               // print-komento tulostaa kokoelman joko osissa tai kokonaan.
               if (komento[0].equals(TULOSTA)) {
                  // Jos komennolla ei ole lisäparametreja, tulostetaan koko kokoelma.    
                  if (komento.length == 1) {
                     korpus.tulostaKokoelmaRiveittain();
                  }    
                  // Jos komennolla on parametreja, tulostetaan valittu dokumentti.
                  else {
                     tunniste = Integer.parseInt(komento[1]);
                     Dokumentti doku = korpus.hae(tunniste);
                     if (doku == null) {
                        System.out.println("Error!");
                     }
                     else {
                        System.out.println(doku); 
                     }
                  }
               }
               // Kun syötteeksi annetaan echo, lisätään komentojen kaiutus.
               else if (komento[0].equals(KAIKU)) {
                  echoLaskuri++;
                  if (echoLaskuri % 2 == 1) {
                     onkoKaiku = true;
                     System.out.println(KAIKU);   
                  }
                  // Jos kaiku 
                  if (echoLaskuri % 2 == 0) {
                     onkoKaiku = false;
                  } 
               }
               // Lisätään kokoelmaan.
               else if (komento[0].equals(LISAA)) {
                  String lisays = komento[1];
                  
                  korpus.lisääKokoelmaan(lisays, nimi);
               }
               // Etsitään hakusanojen avulla.
               else if (komento[0].equals(ETSI)) {
                  String avaimet = komento[1];
                  String[] avainTaulu = avaimet.split("[" + EROTIN + "]");
                  hakusanat = korpus.hakusanatListaan(avainTaulu);
                  korpus.etsiAvaimilla(hakusanat);
               }
               // Poistetaan dokumentti kokoelmasta.
               else if (komento[0].equals(POISTA)) {
                  tunniste = Integer.parseInt(komento[1]);
                  Dokumentti doku = korpus.hae(tunniste);
                  if (doku == null) {
                     System.out.println("Error!");
                  }
                  else {
                     korpus.poistaDokumentti(tunniste);
                  }
               } 
               // Toiminto kokoelman dokumenttien esikäsittelyyn.
               else if (komento[0].equals(ESIKASITTELE)) {
                  String välimerkit = komento[1];
                  if (välimerkit.contains(EROTIN)) {
                     System.out.println("Error!");
                  }
                  else {
                     korpus.kiillotus(sulkusanat, välimerkit);
                  }
               }
               // Ladataan kokoelma uudelleen tiedostosta.
               else if (komento[0].equals(RESETOI)) {
                  korpus.lataaKokoelma(nimi);

               }
               // Käyttäjä antaa syötteeksi quit, käännetään lippumuuttuja ja ajo loppuu               
               else if (komento[0].equals(LOPETUS)) {
                  jatkaAjoa = false;
                     
               }
               // Jos syöte läpäisi rakenteellisen tarkistuksen, mutta ei silti
               // vastaa mitään komennoista, annetaan virheviesti
               else {
                  System.out.println("Error!");   
               } 
            }
            // Siepataan NullPointerException ja NumberFormatException
            catch (NullPointerException | NumberFormatException e) {
               System.out.println("Error!");
               }
            }
            while (jatkaAjoa);
         }
      System.out.println("Program terminated.");  
      }  
   }