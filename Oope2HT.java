
/**
 * Oope2HT-ajoluokka jolla käynnistetään ohjelma 
 * ja kutsutaan käyttöliittymäluokkaa.
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
        
public class Oope2HT { 
 
   /** 
    * Oope2HT-ajoluokan päämetodi. 
    * 
    * Tarkistaa, että komentoriviparametreja on oikea määrä ja käynnistää 
    * käyttöliittymän.
    */     
   public static void main(String[] args) {
      
      // Esitellään ohjelman tarkoitus
      System.out.println("Welcome to L.O.T.");
  
      // Tarkistetaan, että komentoriviparametrit on kunnossa
      if (args.length != 2){
         System.out.println("Wrong number of command-line arguments!"); 
         System.out.println("Program terminated."); 
      }
      // Jos komentoriviparametrit on kunnossa, jatketaan ajoa
      else { 
         // Luodaan ja alustetaan taulukot ensimmäisen kerran
         String nimi = args[0];
         // Ladataan sulkusanat listaan jos ne on syötetty parametriksi.
         String sulkusanaNimi = null;
         if (args.length == 2) {
            sulkusanaNimi = args[1];
         }
         
         Kayttoliittyma.liittyma(nimi, sulkusanaNimi);
         
      }     
   }
}