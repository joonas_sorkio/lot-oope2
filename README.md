# Lot Oope2

Course project for Object-Oriented Programming II. 
Program to manipulate text collections. 
Commands are in english but the code comments are only in finnish as this was a finnish language course. 
Javadoc can be generated from the code.
See instructions.txt.

Kurssin Olio-ohjelmoinnin Perusteet II harjoitustyö.
Ohjelma tekstikokoelmien käsittelyyn.
Komennot englanniksi, mutta kommentit vain suomeksi.
Voidaan muodostaa javadoc. 
Katso instructions.txt
