package harjoitustyo.kokoelma;

import harjoitustyo.dokumentit.Dokumentti;
import harjoitustyo.dokumentit.Vitsi;
import harjoitustyo.dokumentit.Uutinen;
import harjoitustyo.apulaiset.Kokoava;
import harjoitustyo.omalista.OmaLista;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Kokoelma-luokka, jolla voidaan koota ja käsitellä dokumentteja listamuodossa.
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
public class Kokoelma implements Kokoava<Dokumentti> {
   public static final String DOKUEROTIN = "///";
   /** Viite sulkusanoille. */
   public static LinkedList<String> sulkusanat;
   /** Viite hakusanoille */
   public static LinkedList<String> hakusanat;

   /** Viite OmaLista-luokkaan. */
   private OmaLista<Dokumentti> dokumentit;
    
   /*
    * Parametrillinen rakentaja, heittää poikkeuksen jos numero on virheellinen
    */ 
   public Kokoelma() {
      // Yritetään asettaa. Asettajan heittämän poikkeuksen annetaan mennä
      // omia aikojaan rakentajaa kutsuvalle metodille. 
      dokumentit = new OmaLista<Dokumentti>();
   }
   
   /*
    * Aksessorit
    *
    */
    
   public OmaLista<Dokumentti> dokumentit() {
      return dokumentit;
   }
   /**
    * Lisää kokoelmaan käyttöliittymän kautta annetun dokumentin.
    * 
    * @param uusi viite lisättävään dokumenttiin.
    * @throws IllegalArgumentException jos dokumentin vertailu Comparable-rajapintaa
    * käyttäen ei ole mahdollista, listalla on jo equals-metodin mukaan sama
    * dokumentti eli dokumentti, jonka tunniste on sama kuin uuden dokumentin
    * tai parametri on null.
    */
   @Override
   public void lisää(Dokumentti uusi) throws IllegalArgumentException {
      if (uusi == null) {
         throw new IllegalArgumentException("Virheellinen dokumentti!");
      }
      
      for (int ind = 0; ind < dokumentit.size(); ind++) {
         Dokumentti doku = dokumentit.get(ind);
         if ((uusi.equals(doku))) {
         throw new IllegalArgumentException("Dokumentti löytyy jo listalta!");         
         }
      }
      // Jos dokumentti on kunnossa, lisätään se listaan
      dokumentit.lisää(uusi);

   }

   /**
    * Hakee kokoelmasta dokumenttia, jonka tunniste on sama kuin parametrin
    * arvo.
    * 
    * @param tunniste haettavan dokumentin tunniste.
    * @return null jos haettavaa
    * dokumenttia ei löydetty, doku jos vastaavuus löydetty.
    */
   @Override
   public Dokumentti hae(int tunniste) {

      // Haetaan dokumenttilistan alkiot yksitellen läpi 
      // ja vertaillaan tunnistetta      
      for (int ind = 0; ind < dokumentit.size(); ind++) {
         Dokumentti doku = dokumentit.get(ind);
         // Palautetaan välittömästi dokumentti kun löydetään vastaavuus
         if (tunniste == doku.tunniste()) {
            return doku; 
         }
      }
               
      return null;
   }
   
   /**
    * Metodi kokoelman kiillotukseen.
    * 
    * Käytetään hyödyksi Dokumentti-luokan siivoa-metodia.
    * 
    * @param sulkusanat sisältävä linkitetty lista.
    * @param välimerkit sisältävä merkkijono.
    */
   public void kiillotus (LinkedList<String> sulkusanat, String välimerkit) {
      for (int ind = 0; ind < dokumentit.size(); ind++) {
         Dokumentti dokuM = dokumentit.get(ind);
         dokuM.siivoa(sulkusanat, välimerkit);
      }   
   }
   
   /**
    * Metodi kokoelman tulostukseen riveittäin. 
    * 
    * dokumenttia ei löydetty, doku jos vastaavuus löydetty.
    */   
   public void tulostaKokoelmaRiveittain() {
      for (int ind = 0; ind < dokumentit.size(); ind++) {
         Dokumentti dokumenthi = dokumentit.get(ind);
         System.out.println(dokumenthi.toString());
      }
   }
   
   /**
    * Haetaan dokumentteja avainsanojen avulla.
    * 
    * Käytetään hyödyksi dokumentti-luokan sanatTäsmäävät-metodia.
    * 
    * @param hakusanat sisältävä linkitetty lista..
    */   
   public void etsiAvaimilla(LinkedList<String> hakusanat) {
      // Lippumuuttuja täsmäävyydelle. Alustavasti false
      boolean täsmääkö = false;
      // Tulostetaan kaikki dokumentit jotka täsmäävät jokaisen avainsanan kanssa.
      for (int ind = 0; ind < dokumentit.size(); ind++) {
         Dokumentti dokumenthi = dokumentit.get(ind);
         täsmääkö = dokumenthi.sanatTäsmäävät(hakusanat);
         // Jos löydetään täsmäävyys, tulostetaan dokumentti uudelle riville.
         if (täsmääkö) {
            System.out.println(dokumenthi.tunniste());
         }
      }
   }
   
   /** 
    * Lisätään käyttäjän antamat avainsanat linkitettyyn listaan
    *
    * @param avaimet hakusanat 1d-taulukon sisällä.
    * @return null, jos avaimet on null, hakusanat, jos avaimet on lisätty
    * onnistuneesti listaan.
    */
   public static LinkedList<String> hakusanatListaan(String[] avaimet) { 
      if (avaimet == null) {
        return null;
      }
      hakusanat = new LinkedList<String>();
      for (String avaimet1 : avaimet) {
         hakusanat.add(avaimet1);
      }
      return hakusanat;
   }
   
   /** 
    * Muunnetaan sulkusanat sisältävän tiedoston sisältö linkitetyksi listaksi.
    *
    * @param nimi sulkusanat sisältävän tiedoston nimi.
    * @return sulkusanat, jos avaimet on null, hakusanat, jos sulkusanat on lisätty
    * onnistuneesti listaan.
    */
   public static LinkedList<String> lataaSulkusanat(String nimi) {
      // Alustetaan tiedostonlukija
      Scanner tiedostonLukija = null;
         
      // Luodaan uusi linkitetty lista.
      sulkusanat = new LinkedList<String>();
      // Luodaan merkkijono-muuttuja rivin sisältöä varten
      String rivi;
      try {
         // Luodaan tiedostoon liittyvä olio
         File tiedosto = new File(nimi); 
         // Liitetään lukija tiedostoon
         tiedostonLukija = new Scanner(tiedosto);
         
         // Ajetaan silmukka niin kauan kun seuraava rivi on olemassa.
         while (tiedostonLukija.hasNextLine()) {
            // Lisätään tarkasteltava rivi linkitettyyn listaan.
            rivi = tiedostonLukija.nextLine();
            sulkusanat.add(rivi);   
         }  
         // Suljetaan lukija
         tiedostonLukija.close();  
      }
      // Siepataan kaikki poikkeukset, palautetaan null
      catch (Exception e) {
         sulkusanat = null;
         // Suljetaan tiedoston lukija tarvittaessa ja palautetaan tieto virheestä
         if (tiedostonLukija != null) {
            tiedostonLukija.close();
         }
      }
      // Palautetaan kokoelma
      return sulkusanat;
   }  
   
   /** 
    * Ladataan tekstitiedoston sisältö Kokoelma-tyyppiseen muuttujaan, 
    *
    * @param nimi tekstitiedoston nimi.
    * @return korpus, jos kokoelma on luotu onnistuneesti.
    */   
   public void lataaKokoelma(String nimi) {
      // Alustetaan tiedostonlukija
      Scanner tiedostonLukija = null;
      // Luodaan uusi Kokoelma.
      dokumentit = new OmaLista<Dokumentti>();
      // Luodaan merkkijono-muuttuja rivin sisältöä varten
      String rivi;
      try {
         // Luodaan tiedostoon liittyvä olio
         File tiedosto = new File(nimi);    
         // Liitetään lukija tiedostoon
         tiedostonLukija = new Scanner(tiedosto);
         
         while (tiedostonLukija.hasNextLine()) {
            // Sijoitetaan rivi kerrallaan kokoelmaan.
            rivi = tiedostonLukija.nextLine();
            // Lisätään rivi kokoelmaan lisääKokoelmaan -metodin avulla
            lisääKokoelmaan(rivi, nimi);
            
         }  
         // Suljetaan lukija
         tiedostonLukija.close();
         
      }
      // Siepataan kaikki poikkeukset, palautetaan null
      catch (Exception e) {
         dokumentit = null;
         // Suljetaan tiedoston lukija tarvittaessa ja palautetaan tieto virheestä
         if (tiedostonLukija != null) {
            tiedostonLukija.close();
         }
      }
   }
   /** 
    * Metodi yksittäisen rivin kokoelmaan lisäämiseen.  
    *
    * Varmistetaan, että dokumentin alalaji on oikeaa tyyppiä. 
    * Esim. Vitsejä ei voi lisätä Uutisena jne.
    * 
    * @param rivi tarkasteltavan rivin sisältö merkkijonona.
    * @param nimi tekstitiedoston nimi.
    * @return korpus, jos lisäys on onnistunut.
    */   
   public void lisääKokoelmaan(String rivi, String nimi) {
      
      try {
         // Jaetaan nimi osiin, jotta voidaan selvittää helposti dokumentin tyyppi.
         String[] nimiOsat = nimi.split("[" + "_" + "]");   
         String uutiset = "news";
         String vitsit = "jokes";

         // Jaetaan rivi osiin ja sijoitetaan yksiulotteiseen taulukkoon
         String[] riviOsat = rivi.split(Pattern.quote(DOKUEROTIN));
         int tunniste = Integer.parseInt(riviOsat[0]);
         String teksti = riviOsat[2];
         DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy"); 
         
         Dokumentti doku = hae(tunniste);
         
         if (doku == null) {
            // Jos nimen eka osa vastaa uutista, lisätään kokoelmaan.
            if (nimiOsat[0].equals(uutiset)) {    
               String pvmTeksti = riviOsat[1];
               LocalDate pvm = LocalDate.parse(pvmTeksti, formatter);
               Uutinen valhe = new Uutinen(tunniste, pvm, teksti);
               dokumentit.lisää(valhe);
            }  
            // Jos nimen eka osa vastaa vitsiä, lisätään kokoelmaan.
            else if (nimiOsat[0].equals(vitsit)) {
               String laji = riviOsat[1];
               // Katsotaan, että laji ei sisällä muita kuin kirjaimia (esim. pvm)
               if (laji.matches(".*\\d.*")) {
               throw new IllegalArgumentException();
            }
               Vitsi hauska = new Vitsi(tunniste, laji, teksti);
               dokumentit.lisää(hauska);
            } 
         }
         
         else {
         System.out.println("Error!");
         }
      }
      // Siepataan kaikki poikkeukset, palautetaan null
      catch (Exception e) {
         System.out.println("Error!");
      }  
   }
   
   /**
    * Poistetaan haluttu dokumentti kokoelmasta tunnisteen avulla.
    * 
    * @param tunniste haettavan dokumentin tunniste.
    * dokumenttia ei löydetty, doku jos vastaavuus löydetty.
    */   
   public void poistaDokumentti (int tunniste) {
      // Haetaan dokumenttilistan alkiot yksitellen läpi 
      // kunnes löydetään vastaavuus ja poistetaan      
      for (int ind = 0; ind < dokumentit.size(); ind++) {
         Dokumentti doku = dokumentit.get(ind);
         // Poistetaan vastaava dokumentti.
         if (tunniste == doku.tunniste()) {
            dokumentit.remove(ind);
            
         }
      }
   }
   
   /**
    * Korvattu toString-metodi
    * 
    * @return kokoelma-luokan merkkijonoesitys.
    */   
   @Override
   public String toString() {   
      String luokanNimi = getClass().getSimpleName();
      return luokanNimi + " " +  dokumentit;
   }
}
