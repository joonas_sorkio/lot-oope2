package harjoitustyo.dokumentit;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Uutinen-luokka, perii Dokumentti-luokan.
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
public class Uutinen extends Dokumentti {
   /** Päivämäärä-attribuutti */
   private LocalDate päivämäärä;

   /*
    * Kolmiparametrinen rakentaja
    */
   public Uutinen(int uusiTunniste, LocalDate uusiPvm, String uusiTeksti) throws IllegalArgumentException{
       super(uusiTunniste, uusiTeksti);
       päivämäärä(uusiPvm);
   }


   /* 
    * Aksessorit.
    *
    */
   
   public LocalDate päivämäärä() {
      return päivämäärä;
   }

   public void päivämäärä(LocalDate uusiPvm) throws IllegalArgumentException {
       if (uusiPvm == null) {
          throw new IllegalArgumentException();
       }
       päivämäärä = uusiPvm;
   }
   
   /**
    * Korvattu toString-metodi
    * 
    * @return Uutinen-luokan merkkijonoesitys.
    */   
   @Override
   public String toString() {
      // Muodostetaan merkkijonoesitys. Object-luokan metodia ei kutsuta,
      // jotta esitys pysyisi selkeämpänä.
      String pvmFormaatti = päivämäärä.format(DateTimeFormatter.ofPattern("d.M.yyyy"));
      return tunniste() + "///" + pvmFormaatti + "///" + teksti();
   }    
}
