package harjoitustyo.dokumentit;

import java.util.LinkedList;
import java.util.stream.IntStream;
import harjoitustyo.apulaiset.Tietoinen;

/**
 * Abstrakti Dokumentti-luokka, joka sisältää dokumenteille yhteiset ominaisuudet.
 * <p>
 * Harjoitustyö L.O.T. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @author Joonas Sorkio (joonas.sorkio@tuni.fi)
 * Informaatioteknologian ja viestinnän tiedekunta,
 * Tampereen yliopisto.
 */
public abstract class Dokumentti implements Comparable<Dokumentti>, Tietoinen<Dokumentti> {
   
   /** Dokumentin yksilöivä tunnus. */
   private int tunniste;
   /** Dokumentin tekstisisältö. */    
   private String teksti; 

   /*
    * Kaksiparametrinen rakentaja
    */
   public Dokumentti(int uusiTunniste, String uusiTeksti) throws IllegalArgumentException {
      tunniste(uusiTunniste);
      teksti(uusiTeksti);
   }

   /* 
    * Aksessorit.
    *
    */
   
   public int tunniste() {
      return tunniste;
   }

   public void tunniste(int t) throws IllegalArgumentException {
       if (t < 1) {
          throw new IllegalArgumentException("Virheellinen tunniste!");
       }
       tunniste = t;
   }
   
   public String teksti() {
      return teksti;
   }
   
   public void teksti(String tx) throws IllegalArgumentException {
       if (tx == null || tx.length() < 1) {
          throw new IllegalArgumentException("Virheellinen teksti!");
       }       
       teksti = tx;
   }
   
   /**
    * Tutkii sisältääkö dokumentin teksti kaikki siitä haettavat sanat.
    * 
    * @param hakusanat lista dokumentin tekstistä haettavia sanoja.
    * @return täsmääkö, true jos kaikki listan sanat löytyvät dokumentista. Muuten palautetaan
    * false.
    * @throws IllegalArgumentException jos sanalista on tyhjä tai se on null-arvoinen.
    */
   public boolean sanatTäsmäävät(LinkedList<String> hakusanat) throws IllegalArgumentException {
      // Heitetään poikkeus jos hakusanat -lista on null tai 0 alkion mittainen.
      if (hakusanat == null || 1 > hakusanat.size()) {
         throw new IllegalArgumentException("Virheellinen sanalista!");
      }
      int ind = 0;
      boolean täsmääkö = false; 
      // Jaetaan syöte osiin ja sijoitetaan yksiulotteiseen taulukkoon
      String[] sanat = teksti.split("[" + " " + "]");
      int osumia = 0;
      // Selvitetään sanojen vastaavuus kahdella silmukalla
      // Ulompi silmukka määrittää tarkasteltavan sanan paikan listassa
       while (ind < hakusanat.size()) {
         String Li = hakusanat.get(ind);
         int ind2 = 0;
         // Joka kierroksella lippumuuttuja käännetään takaisin falseksi,
         // jotta true palautuisi vain kun jokainen hakusana on löydetty
         ind++;
         boolean katkaisu = false; 
         // Jos hakusana vastaa dokumentin sanaa, käännetään lippumuuttuja todeksi.
         while (!katkaisu && ind2 < sanat.length) {
            String Dj = sanat[ind2];
            int ind3 = 0;
            // jos hakusana löytyy tekstistä, lisätään osumien määrää
            if (Li.equals(Dj)) {
               osumia++;
               katkaisu = true; 
            }
            ind2++;  
         }
      }
      // Osumien määrä vastaa tai ylittää hakusanojen määrän, palautetaan true
      if (osumia >= hakusanat.size()) {
         täsmääkö = true;
      }
      // Palautetaan boolean arvo
      return täsmääkö;     
   }
   
   /**
    * Muokkaa dokumenttia siten, että tiedonhaku on helpompaa ja tehokkaampaa.
    * <p>
    * Metodi poistaa ensin dokumentin tekstistä kaikki annetut välimerkit ja
    * muuntaa sitten kaikki kirjainmerkit pieniksi ja poistaa lopuksi kaikki
    * sulkusanojen esiintymät.
    * <p>
    * 
    * @param sulkusanat lista dokumentin tekstistä poistettavia sanoja.
    * @param välimerkit dokumentin tekstistä poistettavat välimerkit merkkijonona.
    * @throws IllegalArgumentException jos jompikumpi parametri on tyhjä tai null-
    * arvoinen.
    */
   public void siivoa(LinkedList<String> sulkusanat, String välimerkit)
   throws IllegalArgumentException {
      // Heitetään poikkeus jos hakusanat -lista on null tai 0 alkion mittainen.
      if (sulkusanat == null || 1 > sulkusanat.size() || välimerkit == null || välimerkit.length() < 1) {
         throw new IllegalArgumentException("Virheellinen parametri!");
      }

      // Sijoitetaan välimerkit char-taulukkoon
      char[] merkit = välimerkit.toCharArray();
      // Silmukka, jossa korvataan kaikki valitut merkit tyhjällä merkillä.
      for (int ind1 = 0; ind1 < merkit.length ; ind1++) {  
         char merkki = merkit[ind1];
         String merkkiJonoksi = String.valueOf(merkki);
         if (!merkkiJonoksi.matches("^[a-zA-Z]*$")) {
            merkkiJonoksi = "\\" + merkkiJonoksi;    
         }  
         teksti = teksti.replaceAll(merkkiJonoksi, "");
           
      }
      // Korvataan kaksi peräkkäistä välilyöntiä yhdellä.
      String tuplaVali = "  ";
      teksti = teksti.replaceAll(tuplaVali, " ");
      // Poistetaan välilyönti alusta ja lopusta
      teksti = teksti.trim();
      // Isot kirjaimet pieniksi.
      teksti = teksti.toLowerCase();
      
      String[] sanat = teksti.split("[" + " " + "]");
      // Selvitetään sanojen vastaavuus kahdella silmukalla
      // Ulompi silmukka määrittää tarkasteltavan sanan paikan listassa
      int ind = 0;
      while (ind < sulkusanat.size()) {
         String Li = sulkusanat.get(ind);
         int ind2 = 0;
         ind++;
         // Jos hakusana vastaa dokumentin sanaa, poistetaan se.
         while (ind2 < sanat.length) {
            String Dj = sanat[ind2];
            // Jos hakusana vastaa dokumentin sanaa, poistetaan se.
            if (Li.equals(Dj)) {
               sanat = poistaAlkio(sanat, ind2);  
            }
            // Muussa tapauksessa kasvatetaan laskuria.
            else {
               ind2++;
            }
         }
      String sanatLiitos = String.join(" ", sanat);
      teksti = sanatLiitos;
      }
   }
      
   /** 
    * Poistetaan valittu alkio taulusta.
    *
    * Tämä tapahtuu kopioimalla taulukko muuten, paitsi indeksin kohdalla.
    * 
    * @param taulu sisältää olemassa olevat dokumentit.
    * @param indeksi määrittää poistokohdan.
    * @return taulu, jos se on null, uusiTaulu, jos poisto on onnistunut.
    */
   public static String[] poistaAlkio (String[] taulu, int indeksi) { 
  
      if (taulu == null || indeksi < 0 || indeksi >= taulu.length) { 
            return taulu; 
      }  
      String[] uusiTaulu = new String[taulu.length - 1]; 
      // Kopioidaan vanhan taulukon alkiot uuteen, paitsi indeksin määräämässä
      // kohdassa.
      for (int ind = 0, ind2 = 0; ind < taulu.length; ind++) { 
  
         if (ind == indeksi) { 
            continue; 
         } 
         uusiTaulu[ind2++] = taulu[ind]; 
      } 
  
      // Palautetaan uusi taulu
      return uusiTaulu;
   }       
  
   /** 
    * Korvattu equals-metodi dokumenttien vertailuun.
    *
    * @param obj vertailtun kohde.
    * @return true, jos attribuutit samat, false, jos ei.
    */
   @Override
   public boolean equals(Object obj) {
      try {
         // Asetetaan olioon viite dokumentti.
         Dokumentti toinen = (Dokumentti)obj;
         
         // Verrataan tunniste-attribuutteja, equals on true jos attribuutit samat.
         return (tunniste == toinen.tunniste());
      }
      // Poikkeuksissa palautetaan false.
      catch (Exception e) {
         return false;
      }
   }

   /** 
    * Korvattu compareTo-metodi dokumenttien tunnisteiden vertailuun.
    *
    * @param toinen vertailtun kohde.
    * @return -1, jos tämä dokumentti on pienempi, 0, jos dokumentit ovat samoja,
    * 1, jos tämä dokumentti on isompi.
    */
   @Override
   public int compareTo(Dokumentti toinen) {
      // Jos tämän dokumentin tunniste on pienempi kuin toisen niin palautetaan -1.
      if (tunniste < toinen.tunniste) {
         return -1;
      }   
      // Jos tämän dokumentin tunniste on sama kuin toisen niin palautetaan 0.
      else if (tunniste == toinen.tunniste) {
         return 0;
      }
      // Tämän dokumentin tunniste on isompi.
      else {
         return 1;
      }      
   }
 
   /** 
    * Korvattu toString-metodi.
    *
    * @return merkkijonoesitys.
    */
   @Override
   public String toString() {

      // Muodostetaan merkkijonoesitys. Object-luokan metodia ei kutsuta,
      // jotta esitys pysyisi selkeämpänä.
      return tunniste + "///" + teksti;
   }
}
